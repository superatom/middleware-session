<?php

namespace Superatom\Providers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Session\FileSessionHandler;
use Illuminate\Session\Store;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class FileSessionServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['session'] = function () use ($app) {
            $config = $app['config']->get('session');
            $handler = new FileSessionHandler(new Filesystem(), $config['files']);

            return new Store($config['cookie'], $handler);
        };
    }
}
