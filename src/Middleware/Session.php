<?php

namespace Superatom\Middleware;

use Carbon\Carbon;
use Illuminate\Session\Store;
use Superatom\Application;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Session
{
    /**
     * @var Application
     */
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function __invoke(Request $req, Response $res, callable $next)
    {
        if (!$this->sessionConfigured()) {
            return $next($req, $res);
        }

        $session = $this->startSession($req);
        $req->setSession($session);

        $newResponse = $next($req, $res);

        $session->save();
        $this->collectGarbage($session);
        $this->queueSessionCookie($session);

        return $newResponse;
    }

    protected function sessionConfigured()
    {
        return (!is_null($this->getSessionConfig('cookie')));
    }

    protected function startSession(Request $request)
    {
        /** @var Store $session */
        $session = $this->app['session'];
        $session->setId($request->cookies->get($session->getName()));
        $session->start();

        return $session;
    }

    protected function collectGarbage(Store $session)
    {
        $probability = $this->getSessionConfig('gc.probability');
        $divisor = $this->getSessionConfig('gc.divisor');
        if ($this->configHitsLottery($probability, $divisor)) {
            $session->getHandler()->gc($this->getLifeTimeSeconds());
        }
    }

    protected function configHitsLottery($probability, $divisor)
    {
        return (mt_rand(1, $divisor) <= $probability);
    }

    protected function getLifeTimeSeconds()
    {
        return $this->getSessionConfig('lifetime') * 60;
    }

    protected function getCookieLifeTime()
    {
        $expire_on_close = $this->getSessionConfig('expire_on_close');
        $lifetime = $this->getSessionConfig('lifetime');

        return ($expire_on_close ? 0 : Carbon::now()->addMinutes($lifetime));
    }

    protected function queueSessionCookie(Store $session)
    {
        $name = $session->getName();
        $value = $session->getId();
        $lifetime = $this->getCookieLifeTime();
        $path = $this->getSessionConfig('path');
        $domain = $this->getSessionConfig('domain');
        $secure = $this->getSessionConfig('secure', false);

        $this->app->cookie->queue(
            new Cookie($name, $value, $lifetime, $path, $domain, $secure));
    }

    protected function getSessionConfig($key, $default = null)
    {
        return $this->app->config->get('session.'.$key, $default);
    }
}
